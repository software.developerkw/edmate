package com.cstpl.menorahoes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subjects {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subject_title")
    @Expose
    private String subjectTitle;
    @SerializedName("subject_code")
    @Expose
    private String subjectCode;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("maximum_marks")
    @Expose
    private Integer maximumMarks;
    @SerializedName("pass_marks")
    @Expose
    private Integer passMarks;
    @SerializedName("is_lab")
    @Expose
    private Integer isLab;
    @SerializedName("is_elective_type")
    @Expose
    private Integer isElectiveType;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectTitle() {
        return subjectTitle;
    }

    public void setSubjectTitle(String subjectTitle) {
        this.subjectTitle = subjectTitle;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Integer getMaximumMarks() {
        return maximumMarks;
    }

    public void setMaximumMarks(Integer maximumMarks) {
        this.maximumMarks = maximumMarks;
    }

    public Integer getPassMarks() {
        return passMarks;
    }

    public void setPassMarks(Integer passMarks) {
        this.passMarks = passMarks;
    }

    public Integer getIsLab() {
        return isLab;
    }

    public void setIsLab(Integer isLab) {
        this.isLab = isLab;
    }

    public Integer getIsElectiveType() {
        return isElectiveType;
    }

    public void setIsElectiveType(Integer isElectiveType) {
        this.isElectiveType = isElectiveType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}

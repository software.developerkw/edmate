package com.cstpl.menorahoes.activities;


import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.utils.BaseActivity;
import com.cstpl.menorahoes.utils.Utils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    final String TAG = RegisterActivity.class.getSimpleName();
    LinearLayout llAccount;
    EditText ed_register_name, ed_class, ed_register_mobile, edUserName,edEmail,edPassword,edConfirmPassword;
    Button ed_school, ed_address;
    TextView tvRegisterNow,tvTermsConditions,tvPrivacyPolicy;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    ImageView imgShowPassword,imgShowCPassword;

    int pCount=0,cCount=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        llAccount = (LinearLayout)findViewById(R.id.ll_have_account);
        tvRegisterNow = (TextView)findViewById(R.id.tv_register_now);
        ed_register_name = (EditText)findViewById(R.id.ed_register_name);
        ed_class = (EditText)findViewById(R.id.ed_class);
        ed_school = (Button)findViewById(R.id.ed_school);
        ed_address = (Button)findViewById(R.id.ed_address);
        ed_register_mobile = (EditText)findViewById(R.id.ed_register_mobile);
        edPassword = (EditText)findViewById(R.id.ed_register_password);
        edConfirmPassword = (EditText)findViewById(R.id.ed_register_pwd_confirm);
        imgShowCPassword = (ImageView)findViewById(R.id.img_register_show_cpwd);
        imgShowPassword = (ImageView)findViewById(R.id.img_register_show_pwd);


        tvTermsConditions = (TextView) findViewById(R.id.tv_terms_conditions);
        tvPrivacyPolicy = (TextView) findViewById(R.id.tv_privacy_policy);

        llAccount.setOnClickListener(this);
        tvPrivacyPolicy.setOnClickListener(this);
        tvTermsConditions.setOnClickListener(this);

        imgShowPassword.setOnClickListener(this);
        imgShowCPassword.setOnClickListener(this);
        tvRegisterNow.setOnClickListener(this);

        ed_address.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent ;
        switch (v.getId()){
            case R.id.ed_address:
                callPlaceAutocompleteActivityIntent(1001);
                break;
            case R.id.ll_have_account:
                intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_register_now:

                if(ed_register_name.getText().toString().equals("")){
                    Toast.makeText(this, getResources().getString(R.string.plz_enter_name), Toast.LENGTH_SHORT).show();
                }
                /*else if(edUserName.getText().toString().equals("")){
                    Toast.makeText(this, getResources().getString(R.string.plz_enter_user_name), Toast.LENGTH_SHORT).show();
                }
                else if(edEmail.getText().toString().equals("")){
                    Toast.makeText(this, getResources().getString(R.string.plz_enter_email), Toast.LENGTH_SHORT).show();
                }
                else if(!edEmail.getText().toString().matches(emailPattern)){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.invalid_email_pattern), Toast.LENGTH_SHORT).show();
                }*/
                else if(edPassword.getText().toString().equals("")){
                    Toast.makeText(this, getResources().getString(R.string.plz_enter_pwd), Toast.LENGTH_SHORT).show();
                }
                else if(edConfirmPassword.getText().toString().equals("")){
                    Toast.makeText(this, getResources().getString(R.string.plz_enter_confirm_pwd), Toast.LENGTH_SHORT).show();
                }
                else if(!edPassword.getText().toString().equals(edConfirmPassword.getText().toString())){
                    Toast.makeText(this, getResources().getString(R.string.both_pwds_doent_match), Toast.LENGTH_SHORT).show();
                }
                else {

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("name",ed_register_name.getText().toString().trim());
                        jsonObject.put("class",ed_class.getText().toString().trim());
                        jsonObject.put("school_name", "abc"); //ed_school.getText().toString()
                        jsonObject.put("address","ajcc"); //ed_address.getText().toString()
                        jsonObject.put("lat","14.786");
                        jsonObject.put("lang","21.987");
                        jsonObject.put("mobileNumber",ed_register_mobile.getText().toString());


                        jsonObject.put("email","someemail@mail.com");
             //           jsonObject.put("username","dummyUsername");
                        jsonObject.put("password",edPassword.getText().toString().trim());
                        jsonObject.put("password_confirmation",edConfirmPassword.getText().toString().trim());
                        jsonObject.put("device_id",appState.getFCM_Id());

                        if(appState.getNetworkCheck()){
                            registerUser(jsonObject);
                        }
                        else {
                            Toast.makeText(RegisterActivity.this,getResources().getString(R.string.plz_check_network_connection), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                break;

            case R.id.tv_privacy_policy:

                intent = new Intent(this,PrivacyPolicyActivity.class);
                startActivity(intent);

                break;

            case R.id.tv_terms_conditions:

                intent = new Intent(this,TermsConditionsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_register_show_pwd:
                pCount++;
                if (pCount % 2 == 0) {
                    edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    imgShowPassword.setImageResource(R.drawable.eye_on);
                } else {
                    edPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    imgShowPassword.setImageResource(R.drawable.eye_off);
                }
                break;
            case R.id.img_register_show_cpwd:
                cCount++;
                if (cCount % 2 == 0) {
                    edConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    imgShowCPassword.setImageResource(R.drawable.eye_on);
                } else {
                    edConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    imgShowCPassword.setImageResource(R.drawable.eye_off);
                }
                break;
        }

    }

    public void registerUser(JSONObject jsonObject){

        Utils.showProgressDialog(this,"Loggin/Registering...");
        Utils.showProgress();
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Utils.REGISTER, jsonObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {

                            Utils.dissmisProgress();
                            String mssg,userName=null,email=null;
                            int status;
                            JSONObject errorObject = null ;
                            JSONArray jsonArray ;
                            try {
                                status = response.getInt("status");
                                mssg = response.getString("message");

                                if(response.has("errors")){
                                    errorObject = response.getJSONObject("errors");
                                }

                                if(status==1){
                                    Intent i = new Intent(RegisterActivity.this,LoginActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(i);
                                    finish();
                                    Toast.makeText(RegisterActivity.this, mssg, Toast.LENGTH_SHORT).show();
                                }else {

                                    if(errorObject.has("username")){
                                        jsonArray = errorObject.getJSONArray("username");
                                        for(int i=0 ;i<jsonArray.length();i++){
                                            userName = jsonArray.getString(i);
                                        }
                                        Toast.makeText(RegisterActivity.this, userName, Toast.LENGTH_SHORT).show();
                                    }else if(errorObject.has("email")) {
                                        jsonArray = errorObject.getJSONArray("email");
                                        for(int i=0 ;i<jsonArray.length();i++){
                                            email = jsonArray.getString(i);
                                        }
                                        Toast.makeText(RegisterActivity.this, email, Toast.LENGTH_SHORT).show();
                                    }

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }, errorListener) {
        };
        queue.add(jsonObjReq);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(RegisterActivity.this,LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        finish();
    }

    private void callPlaceAutocompleteActivityIntent(int requestCode) {
        try {

            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setCountry("IN")
                    .build();

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, requestCode);

            //PLACE_AUTOCOMPLETE_REQUEST_CODE is integer for request code

        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place:" + place.toString());
                ed_address.setText(place.getName());

               // source = place.getLatLng();


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }
}

package com.cstpl.menorahoes.Network;
import android.content.Intent;

import com.cstpl.menorahoes.model.SubjectsResponse;
import com.cstpl.menorahoes.model.TopicsResponse;
import com.cstpl.menorahoes.utils.Utils;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET(Utils.GET_SUBJECTS_OLD)
    Call<SubjectsResponse> getSubjectsList();

    @FormUrlEncoded
    @POST(Utils.GET_SUBJECTS_OLD)
    Call<TopicsResponse> getTopicsList(@Field("subject_id") Integer id );

}
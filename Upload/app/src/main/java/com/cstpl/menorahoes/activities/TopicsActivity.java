package com.cstpl.menorahoes.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cstpl.menorahoes.Network.ApiInterface;
import com.cstpl.menorahoes.Network.ServiceGenerator;
import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.adapters.CategoriesAdapter;
import com.cstpl.menorahoes.adapters.SubjectsAdapter;
import com.cstpl.menorahoes.adapters.TopicsAdapter;
import com.cstpl.menorahoes.model.Categories;
import com.cstpl.menorahoes.model.Subjects;
import com.cstpl.menorahoes.model.SubjectsResponse;
import com.cstpl.menorahoes.model.TopicDetailsModel;
import com.cstpl.menorahoes.model.Topics;
import com.cstpl.menorahoes.model.TopicsResponse;
import com.cstpl.menorahoes.utils.BaseActivity;
import com.cstpl.menorahoes.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class TopicsActivity extends BaseActivity {

    RecyclerView recyclerView;

    private ArrayList<Topics> topicsList;
    TopicsAdapter adapter;

    TextView tvTitle, noCategories;
    ImageView imgBack;
    Toolbar toolbar;
    Button updateSettings ;
    SearchView searchView ;
    private ArrayList<TopicDetailsModel> topicDetailsModels;
    ApiInterface apiService;
    Integer topic_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exams);

        recyclerView = (RecyclerView) findViewById(R.id.rv_exams_categories);
        imgBack = (ImageView) findViewById(R.id.img_toolbar_back);
        tvTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        noCategories = (TextView) findViewById(R.id.tv_no_exams_categories_list);
        apiService = ServiceGenerator.GetBaseUrl(ApiInterface.class);
        updateSettings = findViewById(R.id.btn_exams_update_settings);
        topic_name=getIntent().getIntExtra("subject_id",-1);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        tvTitle.setText(getString(R.string.topics));


        updateSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TopicsActivity.this,SettingsActivity.class);
                startActivity(intent);

            }
        });

        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (appState.getNetworkCheck()) {
            getTopics();
        } else {

            Toast.makeText(this, getResources().getString(R.string.plz_check_network_connection), Toast.LENGTH_SHORT).show();
        }
    }

    public void getTopics() {
        topicDetailsModels = new ArrayList<>();
        Utils.showProgressDialog(this, "getting subjects");
        Utils.showProgress();
        Call<TopicsResponse> call = apiService.getTopicsList(topic_name);

        call.enqueue(new Callback<TopicsResponse>() {
            @Override
            public void onResponse(Call<TopicsResponse>call, retrofit2.Response<TopicsResponse> response) {
                Log.v("Im in response","Testing");
                Utils.dissmisProgress();
                TopicsResponse topicsResponse = response.body();
                if(topicsResponse!=null && topicsResponse.getStatus()==1) {
                    List<TopicDetailsModel> subjectsList = topicsResponse.getData();
                    if (subjectsList.size() != 0) {
                        adapter = new TopicsAdapter(TopicsActivity.this, topicsList, "exams");
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<TopicsResponse>call, Throwable t) {
                Utils.dissmisProgress();
                Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                Log.e("LoginActivity", t.toString());
            }
        });
       /* topicsList = new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(this);
        Utils.showProgressDialog(this, "");
        Utils.showProgress();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, Utils.TOPICS_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dissmisProgress();
                        String mssg;
                        int status;
                        final JSONArray jsonArray;
                        try {

                            status = response.getInt("status");
                            if (status == 1) {
                                jsonArray = response.getJSONArray("data");
                                noCategories.setVisibility(View.GONE);
                                updateSettings.setVisibility(View.GONE);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Gson gson = new Gson();
                                    Type type = new TypeToken<Topics>() {
                                    }.getType();
                                    Topics myQuestions = gson.fromJson(jsonArray.get(i).toString(), type);
                                    topicsList.add(myQuestions);

                                }

                                if (topicsList.size() != 0) {
                                    adapter = new TopicsAdapter(TopicsActivity.this, topicsList, "exams");
                                    recyclerView.setAdapter(adapter);
                                } else {
                                    noCategories.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                }

                            } else {
                                mssg = response.getString("message");
                                noCategories.setVisibility(View.VISIBLE);
                                noCategories.setText(mssg);
                                updateSettings.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, errorListener) {


        };
        queue.add(jsonObjReq);*/

    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
       /* if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }*/
        super.onBackPressed();
    }
}

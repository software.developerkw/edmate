package com.cstpl.menorahoes.getkey;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.activities.TakeExamActivity;
import com.cstpl.menorahoes.activities.TakeExamSectionWiseActivity;
import com.cstpl.menorahoes.activities.TakeExamSectionWiseTimeActivity;
import com.cstpl.menorahoes.adapters.KeyMultipleChoiceRadioAdapter;
import com.cstpl.menorahoes.adapters.MultipleChoiceRadioAdapter;
import com.cstpl.menorahoes.adapters.SpinnerLanguageAdapter;
import com.cstpl.menorahoes.fragments.BaseFragment;
import com.cstpl.menorahoes.model.KeyTakeExam;
import com.cstpl.menorahoes.model.Options;
import com.cstpl.menorahoes.model.TakeExam;
import com.cstpl.menorahoes.utils.BaseActivity;
import com.cstpl.menorahoes.utils.Utils;
import com.cstpl.menorahoes.view.MathJaxWebView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class KeyExamRadioBoxFragment extends BaseFragment {

    View home_fragment_view;


    RecyclerView recyclerView;
    KeyMultipleChoiceRadioAdapter adapter;
    KeyTakeExam takeExam ;
    List<Options> optionsList;
    String fromWich;
    MathJaxWebView tvQuestion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (home_fragment_view == null) {

            home_fragment_view = inflater.inflate(R.layout.key_exam_multiple_choice,container,false);

        } else {
            ((ViewGroup) home_fragment_view.getParent()).removeView(home_fragment_view);
        }

       initUI();

        return home_fragment_view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    public void initUI(){


        tvQuestion = (MathJaxWebView)home_fragment_view.findViewById(R.id.tv_key_choice_question) ;
        recyclerView = (RecyclerView)home_fragment_view.findViewById(R.id.rv_key_multiple_choice);


        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);

        Bundle bundle = this.getArguments();

        if(bundle != null) {

            takeExam =(KeyTakeExam) bundle.getSerializable("question");

            fromWich = bundle.getString("fromWich");

        }


        tvQuestion.getSettings().setJavaScriptEnabled(true);
        tvQuestion.setText(takeExam.getQuestion());

        optionsList = new ArrayList<>();

        try {
            JSONArray jsonArr = new JSONArray(takeExam.getAnswers());
            for (int i=0;i<jsonArr.length();i++){
                Gson gson = new Gson();
                Type type = new TypeToken<Options>() {}.getType();
                Options myQuestions = gson.fromJson(jsonArr.get(i).toString(), type);
                optionsList.add(myQuestions);
            }

            adapter = new KeyMultipleChoiceRadioAdapter(getActivity(),optionsList,takeExam.getId(),takeExam.getCorrect_answers(),takeExam.getUser_submitted());
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}

package com.cstpl.menorahoes.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.utils.BaseActivity;

public class MySubscriptionsActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner spClass, spToClass, spYears;
    String[] arrayClasses, arrayYears;
    TextView tv_submit;
    ImageView imgBack;

    String sel_class, sel_years;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscriptions);

        spClass = findViewById(R.id.spClass);
        spToClass = findViewById(R.id.spToClass);
        spYears = findViewById(R.id.spYears);
        tv_submit = findViewById(R.id.tv_submit);
        imgBack = (ImageView)findViewById(R.id.img_toolbar_back);

        arrayClasses = getResources().getStringArray(R.array.array_classes);
        arrayYears = getResources().getStringArray(R.array.years_array);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_submit.setOnClickListener(this);

        spClass.setOnItemSelectedListener(this);
        spToClass.setOnItemSelectedListener(this);
        spYears.setOnItemSelectedListener(this);

        fillClasses();
        fillBoards();
    }

    private void fillClasses(){

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arrayClasses);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spClass.setAdapter(aa);
        spToClass.setAdapter(aa);

    }

    private void fillBoards() {

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayYears);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spToClass.setAdapter(aa);


    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()){
            case R.id.spClass:
                sel_class = arrayClasses[i];
                break;
            case R.id.spBoards:
                sel_years = arrayYears[i];
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void submitSubscription(){

    }
}

package com.cstpl.menorahoes.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.utils.BaseActivity;

public class EdMateSettingsActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    TextView tvTitle, tv_submit;
    ImageView imgBack;
    Toolbar toolbar ;
    Spinner spClass, spBoards;

    String sel_class, sel_board;
    String[] arrayClasses, arrayBoards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edmate_settings);

        imgBack = (ImageView)findViewById(R.id.img_toolbar_back);
        tvTitle = (TextView)findViewById(R.id.tv_toolbar_title);
        tv_submit = (TextView)findViewById(R.id.tv_submit);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        spClass = (Spinner) findViewById(R.id.spClass);
        spBoards = (Spinner) findViewById(R.id.spBoards);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitle.setText(getString(R.string.str_nav_settings));

        spClass.setOnItemSelectedListener(this);
        spBoards.setOnItemSelectedListener(this);
        tv_submit.setOnClickListener(this);

        setClasses();
        setBoards();
    }

    private void setClasses(){
        arrayClasses = getResources().getStringArray(R.array.array_classes);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayClasses);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spClass.setAdapter(adapter);
    }

    private void setBoards(){
        arrayBoards = getResources().getStringArray(R.array.array_boards);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayBoards);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBoards.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()){
            case R.id.spClass:
                sel_class = arrayClasses[i];
                break;
            case R.id.spBoards:
                sel_board = arrayBoards[i];
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void saveSettings(){


    }

    @Override
    public void onClick(View view) {
        saveSettings();
    }
}

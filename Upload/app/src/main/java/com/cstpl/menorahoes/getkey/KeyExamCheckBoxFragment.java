package com.cstpl.menorahoes.getkey;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.activities.TakeExamActivity;
import com.cstpl.menorahoes.activities.TakeExamSectionWiseActivity;
import com.cstpl.menorahoes.activities.TakeExamSectionWiseTimeActivity;
import com.cstpl.menorahoes.adapters.KeyMultipleChoiceCheckboxAdapter;
import com.cstpl.menorahoes.adapters.MultipleChoiceCheckboxAdapter;
import com.cstpl.menorahoes.adapters.SpinnerLanguageAdapter;
import com.cstpl.menorahoes.fragments.BaseFragment;
import com.cstpl.menorahoes.model.KeyTakeExam;
import com.cstpl.menorahoes.model.Options;
import com.cstpl.menorahoes.model.TakeExam;
import com.cstpl.menorahoes.utils.BaseActivity;
import com.cstpl.menorahoes.utils.Utils;
import com.cstpl.menorahoes.view.MathJaxWebView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class KeyExamCheckBoxFragment extends BaseFragment {

    View check_box_view;

    MathJaxWebView tvQuestion;
    RecyclerView recyclerView;

    KeyTakeExam takeExam;


    List<Options> optionsList;
    KeyMultipleChoiceCheckboxAdapter adapter;


    String fromWich;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (check_box_view == null) {

            check_box_view = inflater.inflate(R.layout.key_exam_multiple_choice, container, false);


        } else {
            ((ViewGroup) check_box_view.getParent()).removeView(check_box_view);
        }

        initUI();

        return check_box_view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void initUI() {

        tvQuestion = (MathJaxWebView) check_box_view.findViewById(R.id.tv_key_choice_question);
        recyclerView = (RecyclerView) check_box_view.findViewById(R.id.rv_key_multiple_choice);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);

        Bundle bundle = this.getArguments();

        if (bundle != null) {

            takeExam = (KeyTakeExam) bundle.getSerializable("question");
            fromWich = bundle.getString("fromWich");

        }


        optionsList = new ArrayList<>();
        tvQuestion.getSettings().setJavaScriptEnabled(true);
        tvQuestion.setText(takeExam.getQuestion());


        try {
            JSONArray jsonArr = new JSONArray(takeExam.getAnswers());
            JSONObject correctObj, userObj;
            ;
            List<String> correctAnsList = new ArrayList<String>();
            List<String> userAnsList = new ArrayList<String>();
            JSONArray userAnsArray = null;
            for (int i = 0; i < jsonArr.length(); i++) {

                Gson gson = new Gson();
                Type type = new TypeToken<Options>() {
                }.getType();
                Options myQuestions = gson.fromJson(jsonArr.get(i).toString(), type);
                optionsList.add(myQuestions);

            }

            JSONArray jsonArray = new JSONArray(takeExam.getCorrect_answers());

            if (takeExam.getUser_submitted() != null) {
                userAnsArray = new JSONArray(takeExam.getUser_submitted());
            }


            if (userAnsArray != null) {

                for (int i = 0; i < userAnsArray.length(); i++) {

                    userObj = userAnsArray.getJSONObject(i);
                    userAnsList.add(userObj.getString("answer"));
                }

            }

            for (int i = 0; i < jsonArray.length(); i++) {
                correctObj = jsonArray.getJSONObject(i);
                correctAnsList.add(correctObj.getString("answer"));
            }

            adapter = new KeyMultipleChoiceCheckboxAdapter(getActivity(), optionsList, correctAnsList, userAnsList);
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}

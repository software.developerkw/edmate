package com.cstpl.menorahoes.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.model.Topics;
import com.cstpl.menorahoes.utils.Utils;

import java.util.ArrayList;

public class TopicsAdapter extends RecyclerView.Adapter<TopicsAdapter.ViewHolder> {


    Activity context;
    private ArrayList<Topics> categoriesList;
    private ArrayList<Topics> filteredArrayList;
    String fromWhich;

    public TopicsAdapter(Activity context, ArrayList<Topics> categoriesList, String fromWhich) {

        this.context = context;
        this.categoriesList = categoriesList;
        this.filteredArrayList = categoriesList;
        this.fromWhich = fromWhich;

    }


    @Override
    public TopicsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_exam_categories, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopicsAdapter.ViewHolder holder, int position) {

        final Topics categories = categoriesList.get(position);

        holder.name.setText(categories.getTopicName());

        /*if (fromWhich.equals("exams")) {
            if (categories.getImage() != null && !categories.getImage().equals("")) {
                Glide.with(context)
                        .load(Utils.IMAGE_BASE_URL + "exams/categories/" + categories.getImage())
                        .into(holder.img);
            } else {
                Glide.with(context)
                        .load(Utils.IMAGE_BASE_URL + "exams/categories/default.png")
                        .into(holder.img);
            }
        } else {
            if (categories.getImage() != null && !categories.getImage().equals("")) {
                Glide.with(context)
                        .load(Utils.IMAGE_BASE_URL + "lms/categories/" + categories.getImage())
                        .into(holder.img);
            } else {
                Glide.with(context)
                        .load(Utils.IMAGE_BASE_URL + "lms/categories/default.png")
                        .into(holder.img);
            }
        }*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if (fromWhich.equals("lms_series")) {

                    Intent intent = new Intent(context, LMSSeriesCategoryListActivity.class);
                    intent.putExtra("category", categories);
                    context.startActivity(intent);

                } else if (fromWhich.equals("exams")) {
                    Intent intent = new Intent(context, CategoryExamsListActivity.class);
                    intent.putExtra("category", categories);
                    context.startActivity(intent);
                    context.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                } else if (fromWhich.equals("lms")) {
                    Intent intent = new Intent(context, LMSCategoryList.class);
                    intent.putExtra("category", categories);
                    context.startActivity(intent);
                }*/

            }
        });
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_exam_category_name);
            img = (ImageView) itemView.findViewById(R.id.img_exam_category);
        }
    }


}

package com.cstpl.menorahoes.activities;


import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.cstpl.menorahoes.BuildConfig;
import com.cstpl.menorahoes.Network.ApiInterface;
import com.cstpl.menorahoes.Network.ServiceGenerator;
import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.adapters.CategoriesAdapter;
import com.cstpl.menorahoes.adapters.PopularExamsAdapter;
import com.cstpl.menorahoes.adapters.PopularLmsAdapter;
import com.cstpl.menorahoes.adapters.SubjectsAdapter;
import com.cstpl.menorahoes.model.Categories;
import com.cstpl.menorahoes.model.CategoryListLMS;
import com.cstpl.menorahoes.model.PopularExams;
import com.cstpl.menorahoes.model.Subjects;
import com.cstpl.menorahoes.model.SubjectsResponse;
import com.cstpl.menorahoes.utils.BaseActivity;
import com.cstpl.menorahoes.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;


public class MainActivity extends BaseActivity implements View.OnClickListener {

    private NavigationView navigationView;

    ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;

    NestedScrollView scrollView;
    private Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;

    RecyclerView popularExams, popularLMS, rv_subjects;

    PopularExamsAdapter examsAdapter;
    PopularLmsAdapter lmsAdapter;

    CardView imgExams, imgLms, imgExamsSeries, imgLmsSeries, imgNotifications, imgAnalysis;

    TextView tvTitle, appVersion, noPopularExams, noPopularLMS;
    CircleImageView userPic;
    String  profilePic;
    ApiInterface apiService;
    List<PopularExams> popularExamsList;
    List<CategoryListLMS> popularLMSList;
    private ArrayList<Subjects> subjectsList;
    SubjectsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setStatusBarColor(this, R.color.light_violet_color);
        apiService = ServiceGenerator.GetBaseUrl(ApiInterface.class);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        scrollView = (NestedScrollView) findViewById(R.id.nested_scroll_view);
        popularExams = (RecyclerView) findViewById(R.id.rv_popular_exams);
        popularLMS = (RecyclerView) findViewById(R.id.rv_popular_lms);
        rv_subjects = (RecyclerView) findViewById(R.id.rv_subjects);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        rv_subjects.setLayoutManager(gridLayoutManager);


        imgExams = (CardView) findViewById(R.id.img_main_exams);
        imgLms = (CardView) findViewById(R.id.img_main_lms);
        imgAnalysis = (CardView) findViewById(R.id.img_main_analysis);
        imgNotifications = (CardView) findViewById(R.id.img_main_notifications);
        imgExamsSeries = (CardView) findViewById(R.id.img_main_exams_series);
        imgLmsSeries = (CardView) findViewById(R.id.img_main_lms_series);

        noPopularExams = findViewById(R.id.tv_no_popular_exams);

        noPopularLMS = findViewById(R.id.tv_no_popular_lms);

        appVersion = (TextView) findViewById(R.id.tv_app_version);
        appVersion.setText(getString(R.string.vernsion)+" "+ BuildConfig.VERSION_NAME);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setItemIconTintList(null);
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(this, R.color.white_color));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        popularExams.setLayoutManager(mLayoutManager);
        popularExams.setItemAnimator(new DefaultItemAnimator());


        RecyclerView.LayoutManager pLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        popularLMS.setLayoutManager(pLayoutManager);
        popularLMS.setItemAnimator(new DefaultItemAnimator());


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                drawer.closeDrawers();
                displayContentView(menuItem.getItemId());
                return true;
            }
        });

        imgExams.setOnClickListener(this);
        imgLms.setOnClickListener(this);
        imgAnalysis.setOnClickListener(this);
        imgNotifications.setOnClickListener(this);
        imgExamsSeries.setOnClickListener(this);
        imgLmsSeries.setOnClickListener(this);


        if (appState.getNetworkCheck()) {
         //   getPopularRecords();
            getSubjectsList();
        } else {
            Toast.makeText(this, getResources().getString(R.string.plz_check_network_connection), Toast.LENGTH_SHORT).show();
        }

        GradientDrawable popularExams = (GradientDrawable) noPopularExams.getBackground();
        popularExams.setStroke(2, ContextCompat.getColor(this, R.color.orange_300));


        GradientDrawable popularLMS = (GradientDrawable) noPopularLMS.getBackground();
        popularLMS.setStroke(2, ContextCompat.getColor(this, R.color.blue_color));



    }

    @Override
    protected void onStart() {
        super.onStart();

        initDrawer();

        collapsingToolbarLayout.setTitle(appState.getUserName());

    }


    public void initDrawer() {

        View headerview = navigationView.getHeaderView(0);

        headerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyProfileActivity.class);
                startActivity(intent);
                drawer.closeDrawers();
            }
        });


        tvTitle = (TextView) headerview.findViewById(R.id.header_title_name);
        tvTitle.setText(appState.getUserName());
        setTextColorGradient(tvTitle, getResources().getColor(R.color.update_my_profile_primary), getResources().getColor(R.color.update_my_profile));

        userPic = (CircleImageView) headerview.findViewById(R.id.img_header_user_pic);

        if (sharedPreferences.contains("profilePic")) {

            profilePic = sharedPreferences.getString("profilePic", profilePic);

            String imagePath = Utils.USER_PIC_BASE_URL + profilePic;


            if (profilePic != null && !profilePic.equals("null") && !profilePic.equals("")) {
                Glide.with(this)
                        .load(imagePath)
                        .into(userPic);
            } else {
                Glide.with(this)
                        .load(Utils.USER_PIC_BASE_URL + "default.png")
                        .into(userPic);
            }

        } else {

            Glide.with(this)
                    .load(Utils.USER_PIC_BASE_URL + "default.png")
                    .into(userPic);
        }

    }


    private void displayContentView(int id) {

        Intent intent;

        switch (id) {

            case R.id.nav_home :

                if (appState.getNetworkCheck()) {
                   // getPopularRecords();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.plz_check_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.nav_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_activities:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_analysis:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
           /* case R.id.nav_examhistory:
                intent = new Intent(this, ExamsHistoryActivity.class);
                startActivity(intent);
                break;*/
            case R.id.nav_feedback:
                intent = new Intent(this, AddFeedBackActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_bookmarks:
                intent = new Intent(this, TopicsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_share_app:
                shareApp();
                break;
            case R.id.nav_subscription:
                intent = new Intent(this, MySubscriptionsActivity.class);
                startActivity(intent);
                break;

        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            doExitApp();
        }
    }

    public void shareApp() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Menorah OES");
            String sAux = "\nLet me recommend you this application\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName();
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Choose one"));
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()) {

            case R.id.img_main_exams:
                intent = new Intent(this, ExamsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_main_lms:
                intent = new Intent(this, LMSActivity.class);
                startActivity(intent);
                break;
            case R.id.img_main_analysis:
                intent = new Intent(this, AnalysisActivity.class);
                startActivity(intent);
                break;
            case R.id.img_main_notifications:
                intent = new Intent(this, NotificationsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_main_exams_series:
                intent = new Intent(this, ExamsSeriesActivity.class);
                startActivity(intent);
                break;
            case R.id.img_main_lms_series:
                intent = new Intent(this, LMSSeriesActivity.class);
                startActivity(intent);
                break;
        }

    }

    private long exitTime = 0;


    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, R.string.press_again_exit_app, Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    public void getSubjectsList() {

        subjectsList = new ArrayList<>();
        Utils.showProgressDialog(this, "getting subjects");
        Utils.showProgress();
        Call<SubjectsResponse> call = apiService.getSubjectsList();

        call.enqueue(new Callback<SubjectsResponse>() {
            @Override
            public void onResponse(Call<SubjectsResponse>call, retrofit2.Response<SubjectsResponse> response) {
                Log.v("Im in response","Testing");
                Utils.dissmisProgress();
                SubjectsResponse subjectsResponse = response.body();
                if(subjectsResponse!=null && subjectsResponse.getStatus()==1) {
                    List<Subjects> subjectsList = subjectsResponse.getData();
                    if (subjectsList.size() != 0) {
                        adapter = new SubjectsAdapter(MainActivity.this, subjectsList, "exams");
                        rv_subjects.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<SubjectsResponse>call, Throwable t) {
                Utils.dissmisProgress();
                Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                Log.e("LoginActivity", t.toString());
            }
        });


    }

    public void getPopularRecords() {

        popularExamsList = new ArrayList<>();
        popularLMSList = new ArrayList<>();
        Utils.showProgressDialog(this, "Loggin/Registering...");
        Utils.showProgress();
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, Utils.POPULAR_RECORDS + "?user_id=" + getUserID(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dissmisProgress();

                        final JSONArray jsonArrayExams, jsonArrayLMS;
                        int status;

                        try {
                            status = response.getInt("status");

                            if (status == 1) {
                                jsonArrayExams = response.getJSONArray("exam_records");
                                jsonArrayLMS = response.getJSONArray("lms_records");

                                noPopularExams.setVisibility(View.GONE);
                                noPopularLMS.setVisibility(View.GONE);

                                for (int i = 0; i < jsonArrayExams.length(); i++) {
                                    Gson gson = new Gson();
                                    Type type = new TypeToken<PopularExams>() {}.getType();
                                    PopularExams myQuestions = gson.fromJson(jsonArrayExams.get(i).toString(), type);
                                    popularExamsList.add(myQuestions);

                                }

                                for (int i = 0; i < jsonArrayLMS.length(); i++) {
                                    Gson gson = new Gson();
                                    Type type = new TypeToken<CategoryListLMS>() {}.getType();
                                    CategoryListLMS myQuestions = gson.fromJson(jsonArrayLMS.get(i).toString(), type);
                                    popularLMSList.add(myQuestions);

                                }

                                if (popularLMSList.size() != 0) {
                                    popularLMS.setVisibility(View.VISIBLE);
                                    lmsAdapter = new PopularLmsAdapter(MainActivity.this, popularLMSList);
                                    popularLMS.setAdapter(lmsAdapter);
                                } else {
                                    popularLMS.setVisibility(View.GONE);
                                    noPopularLMS.setVisibility(View.VISIBLE);
                                }

                                if (popularExamsList.size() != 0) {
                                    popularExams.setVisibility(View.VISIBLE);
                                    examsAdapter = new PopularExamsAdapter(MainActivity.this, popularExamsList);
                                    popularExams.setAdapter(examsAdapter);
                                } else {
                                    noPopularExams.setVisibility(View.VISIBLE);
                                    popularExams.setVisibility(View.GONE);

                                }

                            } else {
                                popularExams.setVisibility(View.GONE);
                                popularLMS.setVisibility(View.GONE);
                                noPopularExams.setVisibility(View.VISIBLE);
                                noPopularLMS.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, errorListener) {


        };
        queue.add(jsonObjReq);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.dissmisProgress();
    }
}

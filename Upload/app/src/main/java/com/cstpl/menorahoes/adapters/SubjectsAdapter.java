package com.cstpl.menorahoes.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cstpl.menorahoes.R;
import com.cstpl.menorahoes.activities.CategoryExamsListActivity;
import com.cstpl.menorahoes.activities.LMSCategoryList;
import com.cstpl.menorahoes.activities.LMSSeriesCategoryListActivity;
import com.cstpl.menorahoes.activities.TopicsActivity;
import com.cstpl.menorahoes.model.Categories;
import com.cstpl.menorahoes.model.Subjects;
import com.cstpl.menorahoes.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SubjectsAdapter extends RecyclerView.Adapter<SubjectsAdapter.ViewHolder>  {


    Activity context;
    private List<Subjects> subjectsList;
    String fromWhich;

    public SubjectsAdapter(Activity context, List<Subjects> subjectsList, String fromWhich){

        this.context = context ;
        this.subjectsList = subjectsList;
        this.fromWhich=fromWhich;

    }


    @Override
    public SubjectsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subjects, parent, false);

        return new SubjectsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubjectsAdapter.ViewHolder holder, int position) {

        final Subjects subjects = subjectsList.get(position);
        holder.txtSubject.setText(subjects.getSubjectTitle());
        if(position == 0){
            holder.imgSubject.setImageResource(R.drawable.exams);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.analysis_bg));
        }
        if(position == 1){
            holder.imgSubject.setImageResource(R.drawable.lms);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.popular_exams_bg));
        }
        if(position == 2){
            holder.imgSubject.setImageResource(R.drawable.analysis);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.exams_bg));
        }
        if(position == 3){
            holder.imgSubject.setImageResource(R.drawable.notifications);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.notifications_bg));
        }
        if(position == 4){
            holder.imgSubject.setImageResource(R.drawable.lms_series);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.lms_series_bg));
        }
        if(position == 5){
            holder.imgSubject.setImageResource(R.drawable.exam_series);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.exams_series_bg));
        }
        if(position == 6){
            holder.imgSubject.setImageResource(R.drawable.exams);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.analysis_bg));
        }
        if(position == 7){
            holder.imgSubject.setImageResource(R.drawable.lms);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.popular_exams_bg));
        }
        if(position == 8){
            holder.imgSubject.setImageResource(R.drawable.analysis);
            holder.llt_item.setBackground(ContextCompat.getDrawable(context, R.drawable.exams_bg));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              Intent intent= new Intent(context, TopicsActivity.class);
              intent.putExtra("subject_id",subjects.getId());
              context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return subjectsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtSubject;
        ImageView imgSubject;
        LinearLayout llt_item;

        public ViewHolder(View itemView) {
            super(itemView);

            txtSubject = (TextView)itemView.findViewById(R.id.txtSubject);
            imgSubject = (ImageView)itemView.findViewById(R.id.imgSubject);
            llt_item = (LinearLayout) itemView.findViewById(R.id.llt_item);
        }
    }


}

